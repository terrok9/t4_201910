package model.data_structures;

import java.util.Iterator;
import junit.*;
import junit.framework.TestCase;

public class StackTest<T> extends TestCase {
	// -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------
	private Stack<T> prueba;
	// -----------------------------------------------------------------
    // Escenarios
    // -----------------------------------------------------------------
	public void setEscenario1(){
		Stack<T> pruebaArreglo = new Stack<T>(1);
		prueba = new Stack<T>(pruebaArreglo.size());
	}
	public void setEscenario2(){
		Stack<T> pruebaArreglo = new Stack<T>(0);
		prueba = new Stack<T>(pruebaArreglo.size());
	}
	public void setEscenario3(){
		Stack<T> pruebaArreglo = new Stack<T>(5);
		prueba = new Stack<T>(pruebaArreglo.size());
		
	}
	// -----------------------------------------------------------------
    // Casos de prueba
    // -----------------------------------------------------------------
	public void testIterator(){
		setEscenario2();	
		T t = (T) new Object();
		T a = (T) new Object();
		prueba.push(t);
		prueba.push(a);
		Iterator<T> seleccion = prueba.iterator();
		if(!seleccion.hasNext()){
			fail("Error al iterar, no se reconoció un elemento siguiente");
		}
	}
	public void testIsEmpty(){
		setEscenario1();
		assertTrue("No se reconoce un arreglo vacio", prueba.isEmpty());
		
		setEscenario2();
		assertTrue("No se reconoce un arreglo con elementos", !prueba.isEmpty());
		
	}
	public void testSize(){
		setEscenario3();
		assertEquals(5, prueba.size());
	}
	public void testPush(){
		setEscenario3();
		T t = (T) new Object();
		prueba.push(t);
		assertEquals(6, prueba.size());
	}
	public void testPop(){
		setEscenario3();
		prueba.pop();
		assertEquals(4, prueba.size());
	}
}
