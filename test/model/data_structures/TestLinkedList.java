package model.data_structures;

import junit.*;
import junit.framework.TestCase;
import model.data_structures.ILinkedList;
import model.data_structures.LinkedList;

public class TestLinkedList<T> extends TestCase {
	

	public void test() 
	{
		String a = "primero";
		String b = "segundo";
		String c = "tercero";
		String d = "cuarto";
		String e = "quinto";

		LinkedList<String> lista = new LinkedList<T>(a);
		lista.add(b);
		lista.add(c);
		lista.add(d);
		lista.add(e);

		if(lista.size() == 0)
		{
			fail("No agrega.");
		}
		else
		{
			if(!lista.dar(0).equals(a))
			{
				fail("A�adi� mal el primer nodo.");
			}
			else if(!lista.dar(3).equals(e))
			{
				fail("Error");
			}
		}
		
		
		if(lista.isEmpty())
		{
			fail("Error");
		}
		lista.modificar(0, "elemento");
		if(!lista.dar(0).equals("elemento"))
		{
			fail("Error");
		}
		
	}
		
		
		
}
