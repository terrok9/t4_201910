package model.vo;

/**
 * Representation of a MovingViolation object
 */
public class VOMovingViolation implements Comparable<VOMovingViolation> {


private int ObjectId;

private String Location;

private String Date;

private int valor;

private String accidente;

private String violation;
     
public VOMovingViolation(int pObjectId, String pLocation, String pDate, int pValor, String pAccidente, String pViolation){
  ObjectId = pObjectId;
  Location = pLocation;
  Date = pDate;
  valor = pValor;
  accidente =  pAccidente;
  violation = pViolation;
}

/**
 * @return id - Identificador único de la infracción
 */
public int objectId() {
 // TODO Auto-generated method stub
 return ObjectId;
} 


/**
 * @return location - Dirección en formato de texto.
 */
public String getLocation() {
 // TODO Auto-generated method stub
 return Location;
}

/**
 * @return date - Fecha cuando se puso la infracción .
 */
public String getTicketIssueDate() {
 // TODO Auto-generated method stub
 return Date;
}

/**
 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
 */
public int getTotalPaid() {
 // TODO Auto-generated method stub
 return valor;

}

/**
 * @return accidentIndicator - Si hubo un accidente o no.
 */
public String  getAccidentIndicator() {
 // TODO Auto-generated method stub
 return accidente;
}
 
/**
 * @return description - Descripción textual de la infracción.
 */
public String  getViolationDescription() {
 // TODO Auto-generated method stub
 return violation;
}

@Override
public int compareTo(VOMovingViolation arg0) {
	// TODO Auto-generated method stub
	VOMovingViolation primero = this;
	int retorno = -10;	
	String fecha = primero.getTicketIssueDate();
	if(fecha.equals(arg0.getTicketIssueDate())){
		if(primero.objectId() == arg0.ObjectId){
			retorno = 0;
		}
		else if(primero.objectId() > arg0.ObjectId){
			retorno = 1;
		}
		else{
			retorno = -1;
		}
	}
	if(fecha.hashCode() > arg0.getTicketIssueDate().hashCode()){
		retorno = 1;
	}
	else{
		retorno = -1;
	}
	return retorno;
}
}
