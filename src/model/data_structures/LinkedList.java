package model.data_structures;

public abstract class LinkedList<T extends Comparable<T>> implements ILinkedList<T> {


	protected Nodo<T> primero;
	
	protected int size; 
	
	
	public int size() 
	{
		return size;
	}
	
	public T modificar(int indicador, T elemento) throws IndexOutOfBoundsException 
	{
		if(indicador < 0 || indicador >= size())
		{
			throw new IndexOutOfBoundsException();
		}
		
		Nodo<T> nodo = darNodo(indicador);
		T a = nodo.darElemento();
		nodo.modElemento(elemento);
		return a;
		
	}
	public boolean isEmpty() 
	{	
		return primero == null;
	}
	public boolean contains(Object o) 
	{
		boolean contiene = false;
		if(indexOf(o) != -1)
		{
			contiene = true;
		}
		return contiene;	
	}
	public void clear() 
	{
		primero = null;
		size = 0;	
	}
	public int indexOf(Object o) 
	{
		int pos = 0;
		boolean a = false;
		int b = -1;
		Nodo<T> n = primero;
		while(n != null && !a )
		{
			if(n.darElemento().compareTo((T) o) == 0)
			{
				a = true;
				b = pos;
			}
			else
			{
				n = n.darSiguienteNodo();
				pos++;
			}	
		}
		return b;
		
	}
	public T dar(int indicador) throws IndexOutOfBoundsException
	{
		if(indicador < 0 || indicador >= size())
		{
			throw new IndexOutOfBoundsException();
		}
		
		Nodo<T> n = darNodo(indicador);
		
		return n.darElemento();		
	}
	public Nodo<T> darNodo(int index)
	{
		if(index < 0 || index >= size)
		{
			throw new IndexOutOfBoundsException("Se está pidiendo el indice: " + index + " y el tama�o de la lista es de " + size);
		}
		
		Nodo<T> a = primero;
		int pos = 0;
		while(a != null && pos< index)
		{
			a = a.darSiguienteNodo();
			pos++;
		}
		
		return a;
	}


	
	
}
