package model.data_structures;

import java.util.Iterator;

public class Stack<T> implements IStack<T> {

	//atributos
	
	private T[] simple;
	private int N = 0;
	//Stack siempre deja al final para eficiencia top of stack = last element of array

	//Constructos
	public Stack(int capacidad){
		simple =  (T[]) new Object[capacidad];
		if (capacidad == 0){
			capacidad = 1;
		}
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		boolean e = false;
		if(simple.length == 0){
			e = true;
		}
		return e;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return simple.length;
	}

	@Override
	public void push(T t) {
		// TODO Auto-generated method stub
		if(size() == N) resize(2*size());
		simple[N++] =  t;

	}

	private void resize(int capacity) 
	{    T[] copy = (T[]) new String[capacity];    
	for (int i = 0; i < N; i++)       
		copy[i] = simple[i];    
	simple = copy; 
	}

	@Override
	public T pop() {
		// TODO Auto-generated method stub
		return (T) simple[N--];
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		Iterator<T> iter = null;
		for(int i = 0; i < size(); i++){
			iter = (Iterator<T>) simple[i];
		}
		return iter;
	}
}
