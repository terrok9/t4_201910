package model.data_structures;

public interface ILinkedList<T extends Comparable<T>>  {
	
	public boolean add(T a);

	public T modificar(int b ,T a); 

	public boolean eliminar(T a);

	public boolean siguiente();

	public T modActual();
	
	public boolean isEmpty();
	
	public int size();

	public void lists();

	public T dar(T pObtener);

	
	
	
}