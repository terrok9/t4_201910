package controller;

import java.io.FileReader;
import java.util.Scanner;

import com.opencsv.CSVReader;


import model.data_structures.*;
import model.util.Sort;
import model.vo.VOMovingViolation;
import view.MovingViolationsManagerView;

@SuppressWarnings("unused")
public class Controller {

	private MovingViolationsManagerView view;
	private String[] cuatrimestre;
	
	// TODO Definir las estructuras de datos para cargar las infracciones del periodo definido
	private IStack<VOMovingViolation> pilas;
	private IQueue<VOMovingViolation> colas;
	private ILinkedList<VOMovingViolation> lista;
	// Muestra obtenida de los datos cargados 
	private Comparable<VOMovingViolation> [ ] muestra;

	// Copia de la muestra de datos a ordenar 
	private Comparable<VOMovingViolation> [ ] muestraCopia;

	public Controller() {
		view = new MovingViolationsManagerView();
		
		//TODO inicializar las estructuras de datos para la carga de informacion de archivos
		cuatrimestre = new String[4];
		colas = new Queue<VOMovingViolation>();
		pilas = new Stack<VOMovingViolation>(0);
		
	}

	/**
	 * Leer los datos de las infracciones de los archivos. Cada infraccion debe ser Comparable para ser usada en los ordenamientos.
	 * Todas infracciones (MovingViolation) deben almacenarse en una Estructura de Datos (en el mismo orden como estan los archivos)
	 * A partir de estos datos se obtendran muestras para evaluar los algoritmos de ordenamiento
	 * @return numero de infracciones leidas 
	 */
	public int loadMovingViolations(int pNumeroDeCuatrimestre) 
	{
		// TODO
		long startTime = System.currentTimeMillis();
		cuatrimestre = meses(pNumeroDeCuatrimestre);
		int nDatos = 0;
		try
		{
			 CSVReader reader = new CSVReader(new FileReader("data/Moving_Violations_Issued_in_" + cuatrimestre[0]+ "_2018.csv" ));
			 CSVReader reader2 = new CSVReader(new FileReader("data/Moving_Violations_Issued_in_" + cuatrimestre[1]+ "_2018.csv"));
			 CSVReader reader3 = new CSVReader(new FileReader("data/Moving_Violations_Issued_in_" + cuatrimestre[2]+ "_2018.csv"));
			 CSVReader reader4 = new CSVReader(new FileReader("data/Moving_Violations_Issued_in_" + cuatrimestre[3]+ "_2018.csv"));
      String[] nextLine;
      reader.readNext();
      while((nextLine = reader.readNext()) != null)
      {
    	nDatos++;
    	VOMovingViolation a = new VOMovingViolation(Integer.parseInt(nextLine[0]), nextLine[1], nextLine[12], Integer.parseInt(nextLine[8]), nextLine[11], nextLine[14] );
    	System.out.println("prueba de que se cargan los datos, se le imprime el objectID con el total pagado");
    	System.out.println(a.objectId() +" "+a.getTotalPaid());
    	colas.enqueue(a);
    	pilas.push(a);
    	
    	
      }
      String[] nextLine2;
      reader2.readNext();
      while((nextLine2 = reader2.readNext()) != null)
      {
    	  nDatos++;
    	VOMovingViolation a = new VOMovingViolation(Integer.parseInt(nextLine2[0]), nextLine2[1], nextLine2[12], Integer.parseInt(nextLine2[8]), nextLine2[11], nextLine2[14] );
    	System.out.println("prueba de que se cargan los datos, se le imprime el objectID con el total pagado");
    	System.out.println(a.objectId() +" "+a.getTotalPaid());
    	colas.enqueue(a);
    	pilas.push(a);
    	
    	
      }
      String[] nextLine3;
      reader3.readNext();
      while((nextLine3 = reader3.readNext()) != null)
      {
    	  nDatos++;
    	VOMovingViolation a = new VOMovingViolation(Integer.parseInt(nextLine3[0]), nextLine3[1], nextLine3[12], Integer.parseInt(nextLine3[8]), nextLine3[11], nextLine3[14] );
    	System.out.println("prueba de que se cargan los datos, se le imprime el objectID con el total pagado");
    	System.out.println(a.objectId() +" "+a.getTotalPaid());
    	colas.enqueue(a);
    	pilas.push(a);
    	
      }
      String[] nextLine4;
      reader4.readNext();
      while((nextLine4 = reader4.readNext()) != null)
      {
    	  nDatos++;
    	VOMovingViolation a = new VOMovingViolation(Integer.parseInt(nextLine4[0]), nextLine4[1], nextLine4[12], Integer.parseInt(nextLine4[8]), nextLine4[11], nextLine4[14] );
    	System.out.println("prueba de que se cargan los datos, se le imprime el objectID con el total pagado");
    	System.out.println(a.objectId() +" "+a.getTotalPaid());
    	colas.enqueue(a);
    	pilas.push(a);
    	
      }
      reader.close();
      reader2.close();
      reader3.close();
      reader4.close();
		}
    catch(Exception e)
    {
    	e.getMessage();
    }
		long endTime = System.currentTimeMillis();
		long timeTaken = endTime - startTime;
		System.out.println("QuickSort: " + timeTaken);
		return nDatos;
	}
	
	/**
	 * Generar una muestra aleatoria de tamaNo n de los datos leidos.
	 * Los datos de la muestra se obtienen de las infracciones guardadas en la Estructura de Datos.
	 * @param n tamaNo de la muestra, n > 0
	 * @return muestra generada
	 */
	public Comparable<VOMovingViolation> [ ] generarMuestra( int n )
	{
		long startTime = System.currentTimeMillis();
		muestra = new Comparable[ n ];
		
		// TODO Llenar la muestra aleatoria con los datos guardados en la estructura de datos
		long endTime = System.currentTimeMillis();
		long timeTaken = endTime - startTime;
		System.out.println("QuickSort: " + timeTaken);
		return muestra;
		
	}
	
	/**
	 * Generar una copia de una muestra. Se genera un nuevo arreglo con los mismos elementos.
	 * @param muestra - datos de la muestra original
	 * @return copia de la muestra
	 */
	public Comparable<VOMovingViolation> [ ] obtenerCopia( Comparable<VOMovingViolation> [ ] muestra)
	{
		long startTime = System.currentTimeMillis();
		Comparable<VOMovingViolation> [ ] copia = new Comparable[ muestra.length ]; 
		for ( int i = 0; i < muestra.length; i++)
		{    copia[i] = muestra[i];    }
		long endTime = System.currentTimeMillis();
		long timeTaken = endTime - startTime;
		System.out.println("QuickSort: " + timeTaken);
		return copia;
	}
	
	/**
	 * Ordenar datos aplicando el algoritmo ShellSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public void ordenarShellSort( Comparable<VOMovingViolation>[ ] datos ) {
		long startTime = System.currentTimeMillis();
		Sort.ordenarShellSort(datos);
		long endTime = System.currentTimeMillis();
		long timeTaken = endTime - startTime;
		System.out.println("QuickSort: " + timeTaken);
	}
	
	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public void ordenarMergeSort( Comparable<VOMovingViolation>[ ] datos ) {
		long startTime = System.currentTimeMillis();
		Sort.ordenarMergeSort(datos);
		long endTime = System.currentTimeMillis();
		long timeTaken = endTime - startTime;
		System.out.println("QuickSort: " + timeTaken);
	}

	/**
	 * Ordenar datos aplicando el algoritmo QuickSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public void ordenarQuickSort( Comparable<VOMovingViolation>[ ] datos ) {
		long startTime = System.currentTimeMillis();
		Sort.ordenarQuickSort(datos);
		long endTime = System.currentTimeMillis();
		long timeTaken = endTime - startTime;
		System.out.println("QuickSort: " + timeTaken);
	}

	/**
	 * Invertir una muestra de datos (in place).
	 * datos[0] y datos[N-1] se intercambian, datos[1] y datos[N-2] se intercambian, datos[2] y datos[N-3] se intercambian, ...
	 * @param datos - conjunto de datos a invertir (inicio) y conjunto de datos invertidos (final)
	 */
	public void invertirMuestra( Comparable[ ] datos ) {
		// TODO implementar
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < datos.length; i++){
			 Comparable datostemp = datos[i];
			 datos[ i] = datos[datos.length - i];
             datos[ datos.length] = datostemp;
		}
		long endTime = System.currentTimeMillis();
		long timeTaken = endTime - startTime;
		System.out.println("QuickSort: " + timeTaken);
	}
	
	public void run() {
		long startTime;
		long endTime;
		long duration;
		
		int nDatos = 0;
		int nMuestra = 0;
		
		Scanner sc = new Scanner(System.in);
		boolean fin = false;
		
		while(!fin)
		{
			view.printMenu();
			
			int option = sc.nextInt();
			
			switch(option)
			{
				case 1:
					// Cargar infracciones
					nDatos = this.loadMovingViolations(1);
					view.printMensage("Numero infracciones cargadas:" + nDatos);
					break;
					
				case 2:
					// Generar muestra de infracciones a ordenar
					view.printMensage("Dar tamaNo de la muestra: ");
					nMuestra = sc.nextInt();
					muestra = this.generarMuestra( nMuestra );
					view.printMensage("Muestra generada");
					break;
					
				case 3:
					// Mostrar los datos de la muestra actual (original)
					if ( nMuestra > 0 && muestra != null && muestra.length == nMuestra )
					{    
						view.printDatosMuestra( nMuestra, muestra);
					}
					else
					{
						view.printMensage("Muestra invalida");
					}
					break;

				case 4:
					// Aplicar ShellSort a una copia de la muestra
					if ( nMuestra > 0 && muestra != null && muestra.length == nMuestra )
					{
						muestraCopia = this.obtenerCopia(muestra);
						startTime = System.currentTimeMillis();
						this.ordenarShellSort(muestraCopia);
						endTime = System.currentTimeMillis();
						duration = endTime - startTime;
						view.printMensage("Ordenamiento generado en una copia de la muestra");
						view.printMensage("Tiempo de ordenamiento ShellSort: " + duration + " milisegundos");
					}
					else
					{
						view.printMensage("Muestra invalida");
					}
					break;
					
				case 5:
					// Aplicar MergeSort a una copia de la muestra
					if ( nMuestra > 0 && muestra != null && muestra.length == nMuestra )
					{
						muestraCopia = this.obtenerCopia(muestra);
						startTime = System.currentTimeMillis();
						this.ordenarMergeSort(muestraCopia);
						endTime = System.currentTimeMillis();
						duration = endTime - startTime;
						view.printMensage("Ordenamiento generado en una copia de la muestra");
						view.printMensage("Tiempo de ordenamiento MergeSort: " + duration + " milisegundos");
					}
					else
					{
						view.printMensage("Muestra invalida");
					}
					break;
											
				case 6:
					// Aplicar QuickSort a una copia de la muestra
					if ( nMuestra > 0 && muestra != null && muestra.length == nMuestra )
					{
						muestraCopia = this.obtenerCopia(muestra);
						startTime = System.currentTimeMillis();
						this.ordenarQuickSort(muestraCopia);
						endTime = System.currentTimeMillis();
						duration = endTime - startTime;
						view.printMensage("Ordenamiento generado en una copia de la muestra");
						view.printMensage("Tiempo de ordenamiento QuickSort: " + duration + " milisegundos");
					}
					else
					{
						view.printMensage("Muestra invalida");
					}
					break;
											
				case 7:
					// Mostrar los datos de la muestra ordenada (muestra copia)
					if ( nMuestra > 0 && muestraCopia != null && muestraCopia.length == nMuestra )
					{    view.printDatosMuestra( nMuestra, muestraCopia);    }
					else
					{
						view.printMensage("Muestra Ordenada invalida");
					}
					break;
					
				case 8:	
					// Una muestra ordenada se convierte en la muestra a ordenar
					if ( nMuestra > 0 && muestraCopia != null && muestraCopia.length == nMuestra )
					{    
						muestra = muestraCopia;
						view.printMensage("La muestra ordenada (copia) es ahora la muestra de datos a ordenar");
					}
					break;

				case 9:
					// Invertir la muestra a ordenar
					if ( nMuestra > 0 && muestra != null && muestra.length == nMuestra )
					{    
						this.invertirMuestra(muestra);
						view.printMensage("La muestra de datos a ordenar fue invertida");
					}
					else
					{
						view.printMensage("Muestra invalida");
					}

					break;
					
				case 10:	
					fin=true;
					sc.close();
					break;
			}
		}
	}
	public String[] meses(int pNumeroDeCuatrimestre){
		long startTime = System.currentTimeMillis();
		if(pNumeroDeCuatrimestre == 1){
			cuatrimestre[0] = "January";
			cuatrimestre[1] = "February";
			cuatrimestre[2] = "March";
			cuatrimestre[3] = "April";
		}
		else if(pNumeroDeCuatrimestre == 2){
        	cuatrimestre[0] = "May";
        	cuatrimestre[1] = "June";
        	cuatrimestre[2] = "July";
        	cuatrimestre[3] = "August";
		}
        else{
        	cuatrimestre[0] = "September";
        	cuatrimestre[1] = "October";
        	cuatrimestre[2] = "November";
        	cuatrimestre[3] = "December";
        }
		long endTime = System.currentTimeMillis();
		long timeTaken = endTime - startTime;
		System.out.println("QuickSort: " + timeTaken);
		return cuatrimestre;
	}

	

}
